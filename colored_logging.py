from loguru import logger

logger.debug("This is a debug message!")
logger.info("This is an info logging message!")
logger.warning("This is a waring and should appear in yellow color!")
logger.error("This is an error in red color!")
logger.critical("This is a very critical message!")
